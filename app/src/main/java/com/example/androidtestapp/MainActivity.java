package com.example.androidtestapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

public class MainActivity extends AppCompatActivity {

    EditText userName,passWord;
    Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppCenter.start(getApplication(), "9dba8167-34b6-4b32-8e2d-236faa3d388b",
                Analytics.class, Crashes.class);

        userName = findViewById(R.id.ETUserName);
        passWord = findViewById(R.id.ETpassWord);
        login = findViewById(R.id.BLogin);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userName.getText().toString().equals("test") && passWord.getText().toString().equals("test123")){
                    Intent intent = new Intent(getApplicationContext(),MainActivity2.class);
                    startActivity(intent);
                    finish();
                }else if(userName.getText().toString().equals("") ||userName.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(),"enter username",Toast.LENGTH_SHORT).show();
                }else if(passWord.getText().toString().equals("") ||passWord.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(),"enter password",Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getApplicationContext(),"Invalid credential",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}